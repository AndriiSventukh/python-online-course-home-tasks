# 012_HW_Linux  

Being in working directory we can run ls command with several options that can give us  
a little bit different result in some cases. For example ls -l gives us the list of existing  
in the directory files and other directories in long format including permissions.  
By contrast simple ls command gives us short list formatted linearly.  
The same result will give ls ~, but concerning home directory, we currently staying in.  
![image](img/001.png)  

ls -a additionally gives us an opportunity also to look at hidden files, starting with dot-symbol.
And ls -la shows the same list in long format accordingly. 
The last option -lda ~ enable us to get a look at list of directories (including hidden) in  
long format existing in home directory.  
![image](img/002.png)

On the next step we perform quite simple manipulations with directories and files. We have made test directory  
and file test.txt containing text "test.txt" in it. Then we've made test2 directory and moved previous file to it.
Then via command mv we've rename existing file for test2.txt, copied it to previous directory, changed current  
for .. and removed test2.txt in it. But we failed to remove test2 directory with rmdir, because it containes file.
![image](img/003.png)

Using the cat command we just need to remember that it's functionality is pretty simple - to redirect  
data from somewhere to other somewhere and we are free to choose source and destination with according  
options. At the beginning we forwarded information from etc/fstab to standard output as it stands.
But if the file is fairly big, we can use more instead of cat to review it page by page from the very beginning.
Using less gives us more opportunities comparing to more.  
![image](img/004.png)  

Next step concerns some operations for creating file, writing some text to it. But the most interesting  
part is creating hardlink and symlink. It turns out that file will be existing till the last hard link to  
it exists. But in case with symlink, it becomes idle after removing referenced file.
![image](img/005.png)  

Chmod gives an opportunity to manage file mode permissions. In our case we've made file executable and run it.  
![image](img/006.png)

