def swap_words(text_s: str) -> str:
    str_list = list(text_s.split(' '))
    str_list.insert(str_list.index('reasonable'), 'ins_element1')
    str_list.remove('reasonable')
    str_list.insert(str_list.index('unreasonable'), 'ins_element2')
    str_list.remove('unreasonable')
    str_list.insert(str_list.index('ins_element1'), 'unreasonable')
    str_list.remove('ins_element1')
    str_list.insert(str_list.index('ins_element2'), 'reasonable')
    str_list.remove('ins_element2')
    return ' '.join(str_list)


if __name__ == '__main__':
    print(swap_words('The reasonable man adapts himself to the world; '
                     'the unreasonable one persist in trying to adapt the world to himself.'))

