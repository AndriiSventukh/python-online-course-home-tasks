def fizz_buzz(x: int) -> str:
    if 1 <= x <= 100:
        return ('Fizz' if x % 3 == 0 else '') + \
               ('Buzz' if x % 5 == 0 else '') + \
               (str(x) if x % 5 != 0 and x % 3 != 0 else '')
    return f'{x} is out of range [1, 100]'


if __name__ == '__main__':
    x = int(input('Enter x: '))

    print(fizz_buzz(x))
