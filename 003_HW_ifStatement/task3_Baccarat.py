def result_count(x: str, y: str) -> str:
    card_values = {'2': 2,
                   '3': 3,
                   '4': 4,
                   '5': 5,
                   '6': 6,
                   '7': 7,
                   '8': 8,
                   '9': 9,
                   '10': 0,
                   'j': 0,
                   'J': 0,
                   'q': 0,
                   'Q': 0,
                   'k': 0,
                   'K': 0,
                   'a': 1,
                   'A': 1
                   }
    if x in card_values.keys() and y in card_values.keys():
        return str((card_values[x] + card_values[y]) % 10)
    return f'Do not cheat! ' \
           f'\nThe set of inputted values \"{x}\" and \"{y}\" is not allowed!'


if __name__ == '__main__':
    x = input('Enter x: ')
    y = input('Enter y: ')

    print(result_count(x, y))
