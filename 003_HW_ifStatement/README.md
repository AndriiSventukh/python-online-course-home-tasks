# 003_HW_ifStatement

Task 1 has been done taking into account the fact that:
- two non-horizontal lines can be represented with the equations y = x and y = -x,
- horizontal line can be represented with the equation y = 1.
So that, evaluating wheather is particular point belongs to shadowed area turns out simplier that it could be supposed. 

Task 2 has been done using a modified if else and string concatenation.

Task 3 has been done using a dictionary. Firstly, programm checks whether the set of cards meets requirements, and only after that, the result is calculated.