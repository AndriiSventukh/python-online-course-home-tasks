def sort_names_in_file(src_file_path, dst_file_path: str):
    with open(src_file_path, 'r') as src_file:
        content = src_file.read()
    with open(dst_file_path, 'w') as dst_file:
        dst_file.write('\n'.join(sorted(content.splitlines())))


if __name__ == '__main__':
    sort_names_in_file('data/unsorted_names.txt', 'data/sorted_names.txt')




