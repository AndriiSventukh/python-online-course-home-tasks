def most_common_words(file_path, number_of_words) -> list:
    with open(file_path, 'r') as src_file:
        content = src_file.read()
        content = content.replace(',', '').replace('.', '').lower().split()
        content_table = [[content.count(word), word] for word in set(content)]
        content_table.sort(key=lambda x: x[1])
        content_table.sort(key=lambda x: x[0], reverse=True)
        res = []
        for i in range(number_of_words):
            res.append(content_table[i][1])
    return res


if __name__ == '__main__':
    print(most_common_words('data/lorem_ipsum.txt', 3))




