import csv


def get_top_performers(file_path, number_of_students) -> list:
    with open(file_path, 'r') as src_file:
        content = src_file.read().splitlines()[1::]
        content = [[data[0], int(data[1]), float(data[2])] for data in
                   [[a for a in line.split(',')] for line in content]]
        content = sorted(content, key=lambda x: x[2], reverse=True)
    return [content[i][0] for i in range(number_of_students) if len(content) >= number_of_students]


def write_csv_in_age_descending_order(src_file_path, dst_file_path):
    with open(src_file_path, 'r') as src_file:
        csv_reader = csv.reader(src_file, delimiter=',')
        csv_arr = [row for row in csv_reader]
        columns = csv_arr.pop(0)
        csv_arr = [[row[0], int(row[1]), float(row[2])] for row in csv_arr]
        csv_arr = sorted(csv_arr, key=lambda x: x[1])
        csv_arr.insert(0, columns)

    with open(dst_file_path, 'w') as dst_file:
        csv_writer = csv.writer(dst_file, delimiter=',', lineterminator='\r')    # for row in csv_arr:
        csv_writer.writerows(csv_arr)


if __name__ == '__main__':
    print(get_top_performers('data/students.csv', 5))
    write_csv_in_age_descending_order('data/students.csv', 'data/students(age_descending).csv')
