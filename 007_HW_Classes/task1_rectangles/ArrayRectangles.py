from task1_rectangles.Rectangle import Rectangle


class ArrayRectangles:
    def __init__(self, n=10, *args):
        self.__rectangle_array = [None for i in range(n)]
        if args:
            args = [i for i in args]
            for item in args:
                if isinstance(item, Rectangle):
                    self.AddRectangle(item)
                else:
                    raise TypeError

    def AddRectangle(self, rectangle: Rectangle):
        for i in range(len(self.__rectangle_array)):
            if self.__rectangle_array[i] is None:
                self.__rectangle_array[i] = rectangle
                return True
        else:
            return False

    def NumberMaxArea(self):
        max_area = 0
        for rectangle in self.__rectangle_array:
            if not (rectangle is None):
                if rectangle.Area() > max_area:
                    max_area = rectangle.Area()
        return max_area

    def NumberMinPerimeter(self):
        min_perimeter = 0
        first_save_flag = 1
        for rectangle in self.__rectangle_array:
            if not (rectangle is None):
                if first_save_flag:
                    min_perimeter = rectangle.Perimeter()
                    first_save_flag = 0
                else:
                    if rectangle.Perimeter() < min_perimeter:
                        min_perimeter = rectangle.Perimetr()
        return min_perimeter

    def NumbreSquare(self):
        cout_squares = 0
        for rectangle in self.__rectangle_array:
            if not (rectangle is None):
                if rectangle.IsSquare():
                    cout_squares += 1
        return cout_squares

    def __str__(self):
        return_str = '['
        for item in self.__rectangle_array:
            if item is not None:
                return_str += str(item) + ', '
            else:
                return_str += 'None' + ', '
        return_str = ''.join(return_str[:-2:])
        return_str += ']'
        return return_str
