class Rectangle:

    def __init__(self, *sides):
        sides = [i for i in sides]
        if sides:
            if len(sides) - 1:
                self.__sideA = sides[0]
                self.__sideB = sides[1]
            else:
                self.__sideA = sides[0]
                self.__sideB = 5
        else:
            self.__sideA = 4
            self.__sideB = 3

    def GetSideA(self):
        return self.__sideA

    def GetSideB(self):
        return self.__sideB

    def Area(self):
        return self.__sideA * self.__sideB

    def Perimeter(self):
        return 2 * (self.__sideA + self.__sideB)

    def IsSquare(self):
        return self.__sideA == self.__sideB

    def ReplaceSides(self):
        self.__sideA, self.__sideB = self.__sideB, self.__sideA

    def __str__(self):
        return f'R(A:{self.GetSideA()}, B:{self.GetSideB()})'
