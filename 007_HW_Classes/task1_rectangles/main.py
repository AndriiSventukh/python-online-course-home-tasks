from task1_rectangles.Rectangle import Rectangle
from task1_rectangles.ArrayRectangles import ArrayRectangles

if __name__ == '__main__':
    # Low requirements
    rectangle_dict = {1: Rectangle(),
                      2: Rectangle(8),
                      3: Rectangle(4, 4),
                      4: Rectangle(5)}
    for key in rectangle_dict:
        print(f'Side A fo triangle {key} = {rectangle_dict[key].GetSideA()}')
    for key in rectangle_dict:
        print(f'Side B of triangle {key} = {rectangle_dict[key].GetSideB()}')

    print()
    for key in rectangle_dict:
        rectangle_dict[key].ReplaceSides()
    for key in rectangle_dict:
        print(f'Side A of triangle {key} = {rectangle_dict[key].GetSideA()}')
    for key in rectangle_dict:
        print(f'Side B of triangle {key} = {rectangle_dict[key].GetSideB()}')

    print()
    for key in rectangle_dict:
        print(f'Area of triangle {key} = {rectangle_dict[key].Area()}')

    print()
    for key in rectangle_dict:
        print(f'Perimeter of triangle {key} = {rectangle_dict[key].Perimeter()}')

    print()
    for key in rectangle_dict:
        print(f'Triangle {key} IsSquare = {rectangle_dict[key].IsSquare()}')

    # Advanced requirements
    rectangle_array = ArrayRectangles(5, Rectangle(), Rectangle(8), Rectangle(4, 4), Rectangle(5))
    # # Also possible variant:
    # rectangle_array = ArrayRectangles(5)
    # for key, value in rectangle_dict.items():
    #     rectangle_array.AddRectangle(value)

    print(f'\nrectangle_array after initialization with existing dictionary: {rectangle_array}')

    print(f'\nIs new rectangle (Rectangle(10, 10)) added to rectangle_array?'
          f' - {rectangle_array.AddRectangle(Rectangle(10, 10))}')
    print(f'Is new rectangle (Rectangle()) added to rectangle_array?'
          f' - {rectangle_array.AddRectangle(Rectangle())}')

    print(f'\nRectangle array: {rectangle_array}')

    print(f'\nThe maximum area is: {rectangle_array.NumbreSquare()}')
    print(f'The minimum perimeter is: {rectangle_array.NumberMinPerimeter()}')
    print(f'The number of squares is: {rectangle_array.NumbreSquare()}')
