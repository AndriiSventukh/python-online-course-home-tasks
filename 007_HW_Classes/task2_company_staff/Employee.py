from abc import ABCMeta, abstractmethod


class Employee(metaclass=ABCMeta):

    def __init__(self, *param_set):
        param_set = [it for it in param_set]
        self.__name = param_set[0]
        self.__salary = param_set[1]
        self._bonus = 0

    @property
    def Name(self):
        return self.__name

    @property
    def Salary(self):
        return self.__salary

    @Salary.setter
    def Salary(self, value):
        self.__salary = value

    @abstractmethod
    def SetBonus(self):
        pass

    def ToPay(self):
        return self.__salary + self._bonus

    def __str__(self):
        return f'Name: {self.Name} (s:{self.Salary}, b:{self._bonus}, payment:{self.ToPay()})'
