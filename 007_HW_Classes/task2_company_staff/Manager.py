from task2_company_staff.Employee import Employee


class Manager(Employee):

    def __init__(self, name, salary, quantity):
        self.__quantity = quantity
        self.__bonus = 0
        super().__init__(name, salary)

    def SetBonus(self, bonus):
        if 100 < self.__quantity <= 150:
            self._bonus = bonus + 500
        elif self.__quantity > 150:
            self._bonus = bonus + 1000
        else:
            self._bonus = bonus
