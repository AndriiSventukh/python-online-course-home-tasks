from task2_company_staff.Employee import Employee


class Company:
    def __init__(self, *employees):
        if employees:
            self.__staff = [value for value in employees]
        else:
            self.__staff = []

    def GiveEverybodyBonus(self, companyBonus):
        if self.__staff:
            for employee in self.__staff:
                employee.SetBonus(companyBonus)

    def TotalToPay(self):
        if self.__staff:
            return sum([i.ToPay() for i in self.__staff])

    def NameMaxSalary(self):
        if self.__staff:
            temp_dict = {i.ToPay(): i.Name for i in self.__staff}
            max_sal = max(temp_dict.keys())
            return temp_dict[max_sal]

    def __str__(self):
        return_str = '['
        for item in self.__staff:
            return_str += str(item) + ', '
        return_str = ''.join(return_str[:-2:])
        return_str += ']'
        return return_str
