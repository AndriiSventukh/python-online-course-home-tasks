from task2_company_staff.Employee import Employee


class SalesPerson(Employee):

    def __init__(self, name, salary, percent):
        self.__percent = percent
        self.__bonus = 0
        super().__init__(name, salary)

    def SetBonus(self, bonus):
        if 100 < self.__percent <= 200:
            self._bonus = bonus * 2
        elif self._percent > 200:
            self._bonus = bonus * 3
        else:
            self._bonus = bonus

