from task2_company_staff.Manager import Manager
from task2_company_staff.SalesPerson import SalesPerson
from task2_company_staff.Company import Company

if __name__ == '__main__':
    Nick = SalesPerson('Nick', 2000, 135)
    Nick.SetBonus(200)
    print(f'Company have to pay {Nick.ToPay()} dollars to Nick')

    Mike = Manager('Nick', 3000, 175)
    Mike.SetBonus(500)
    print(f'Company have to pay {Mike.ToPay()} dollars to Mike')

    roga_i_kopyta = Company(Nick, Mike)
    print(f'\n{roga_i_kopyta}')

    roga_i_kopyta.GiveEverybodyBonus(1000)
    print(f'\nAfter giving company bonuses:\n{roga_i_kopyta}')

    print(f'\nTotal payment to staff: {roga_i_kopyta.TotalToPay()}')

    print(f'\n{roga_i_kopyta.NameMaxSalary()} has the maximum salary, including bonuses.')

