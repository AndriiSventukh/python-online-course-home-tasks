a = 4.5
b = 5.9
c = 9
triangleArea = ((a+b+c) * (b+c-a) * (a+c-b) * (a+b-c) / 16) ** (1/2);
print(f'The area of current triangle with sides {a}, {b} and {c} is{triangleArea : .2f}')
