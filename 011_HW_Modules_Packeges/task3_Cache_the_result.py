class CallOnce:
    def __init__(self):
        self.cached_result = 0

    def __call__(self, fn):
        def cached_func(*args):
            if self.cached_result:
                return self.cached_result
            self.cached_result = fn(*args)
            return self.cached_result
        return cached_func


def call_once(fn):
    cached_result = 0

    def cached_func(*args):
        nonlocal cached_result
        if cached_result:
            return cached_result
        cached_result = fn(*args)
        return cached_result

    return cached_func


@call_once
def sum_of_numbers(a, b):
    return a + b


@CallOnce()
def sum_of_numbers2(a, b):
    return a + b


if __name__ == '__main__':
    print(sum_of_numbers(13, 42))
    print(sum_of_numbers(999, 100))
    print(sum_of_numbers(134, 412))
    print(sum_of_numbers(856, 232))

    print('______________')

    print(sum_of_numbers2(13, 42))
    print(sum_of_numbers2(999, 100))
    print(sum_of_numbers2(134, 412))
    print(sum_of_numbers2(856, 232))
