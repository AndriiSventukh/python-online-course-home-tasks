a = "I am global variable!"


def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        # step 3. p.2.2 We can use nonlocal to approach var a from enclosing function, but only before
        # using or initialization the variable with the same name, that shadows higher level scope variable
        nonlocal a
        print(a)
        # this can work as local and nonlocal variable (without previous statement)
        a = "I am local variable!"
        print(a)

        # the second step is to print global a (p.2.1). We can't use global statement after initialization of
        # local variable a, so we can use globals() to approach global variable.
        print(globals()['a'])

    # the first step. p.1.1. from task is to find a way to call inner function
    return inner_function()
