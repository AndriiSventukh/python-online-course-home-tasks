class LastResultRememberer:
    def __init__(self):
        self.last_result = None

    def __call__(self, fn):
        def remember_func(*args):
            print(f'Last result = {self.last_result}')
            self.last_result = fn(*args)
            return self.last_result
        return remember_func


def remember_result(fn):
    last_result = None

    def remember_func(*args):
        nonlocal last_result
        print(f'Last result = {last_result}')
        last_result = fn(*args)
        return last_result
    return remember_func


@remember_result
def sum_list(*args):
    result = ''.join(str(item) for item in args)
    print(f'Current result = {result}')
    return result


@LastResultRememberer()
def sum_list2(*args):
    result = ''.join(str(item) for item in args)
    print(f'Current result = {result}')
    return result


if __name__ == '__main__':
    sum_list("a", "b")
    sum_list("abc", "cde")
    sum_list(3, 4, 5)

    print('______________')

    sum_list2("a", "b")
    sum_list2("abc", "cde")
    sum_list2(3, 4, 5)