def calculate_factorial_loop(num: int) -> int:
    factorial = 1
    while num > 0:
        factorial *= num
        num -= 1
    return factorial


def calculate_factorial_recursively(num: int) -> int:
    return num * calculate_factorial_recursively(num-1) if num > 1 else 1


if __name__ == '__main__':
    n = int(input('Please, enter the number: '))
    print(f'Factorial of {n} is {calculate_factorial_loop(n)}')
    print(f'Factorial of {n} is {calculate_factorial_recursively(n)}')

