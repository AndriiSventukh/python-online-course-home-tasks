def calculate_fibonacci(num: int) -> list:
    fib_sequence = [0]
    if num > 1:
        fib_sequence.append(1)
        num -= 2
        while num:
            fib_sequence.append(fib_sequence[-1] + fib_sequence[-2])
            num -= 1
    return fib_sequence


def calculate_fibonacci_sum(fib_sequence: list) -> int:
    sum_fib = 0
    for n in fib_sequence:
        sum_fib += n
    return sum_fib


def calculate_fibonacci_recursively(num: int) -> list:
    if num > 2:
        fib_sequence = calculate_fibonacci_recursively(num - 1) if num > 2 else [0, 1] if num == 2 else [0]
        fib_sequence.append(fib_sequence[-1] + fib_sequence[-2])
        return fib_sequence
    elif num == 2:
        return [0, 1]
    return [0]


def calculate_fibonacci_sum_recursively(fib_sequence: list) -> int:
    return (fib_sequence.pop(-1) + calculate_fibonacci_sum_recursively(fib_sequence)) if fib_sequence else 0



if __name__ == '__main__':
    n = int(input('Please enter the length of sequence: '))

    sequence = calculate_fibonacci(n)
    print(f'Fibonacci sequence is {sequence} \n'
          f'Sum of elements in Fibonacci sequence is {calculate_fibonacci_sum(sequence)}')

    print()

    sequence = calculate_fibonacci_recursively(n)
    print(f'Fibonacci sequence is {sequence} \n'
          f'Sum of elements in Fibonacci sequence is {calculate_fibonacci_sum_recursively(sequence)}')

