def create_binary_representation(num: str) -> str:
    num = int(num)
    if num == 0:
        return '0'
    max_reg = 0
    while num - 2 ** max_reg >= 0:
        max_reg += 1

    bin_repr_str = []
    while max_reg > 0:
        if num - 2**(max_reg - 1) >= 0:
            bin_repr_str.append('1')
            num -= 2 ** (max_reg - 1)
        else:
            bin_repr_str.append('0')
        max_reg -= 1

    return ''.join(bin_repr_str)


def calculate_ones_sum(num_str: str) -> int:
    return num_str.count('1')


def create_binary_representation_recursively(num: str) -> str:
    return create_binary_representation_recursively(int(num) // 2) + str(int(num) % 2) if num else ''


def calculate_ones_sum_recursively(num_str: str) -> int:
    str_list = [ch for ch in num_str]
    return (int(str_list.pop(-1)) + calculate_ones_sum_recursively(''.join(str_list))) if num_str else 0


if __name__ == '__main__':
    n = input('Please input the number: ')

    print(f'Number is {n}, binary representation is {create_binary_representation(n)}, '
          f'sum is {calculate_ones_sum(create_binary_representation(n))}')

    print()

    print(f'Number is {n}, binary representation is {create_binary_representation_recursively(n)}, '
          f'sum is {calculate_ones_sum_recursively(create_binary_representation(n))}')
