def matrix_transposition_loop(matrix: list) -> list:
    transposed_matrix = [[0 for i in range(len(matrix))] for j in range(len(matrix[0]))]
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            transposed_matrix[j][i] = matrix[i][j]
    return transposed_matrix


def matrix_print(matrix: list):
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            print("%4d" % matrix[i][j], end='')
        print()
    print()


if __name__ == '__main__':
    matrix1 = [[1, 2], [3, 4], [5, 6]]
    matrix2 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    matrix3 = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
    matrix_list = [matrix1, matrix2, matrix3]

    for matrix_i in matrix_list:
        print('Original matrix is:')
        matrix_print(matrix_i)

        matrix_i = matrix_transposition_loop(matrix_i)

        print('Transposed matrix is:')
        matrix_print(matrix_i)

