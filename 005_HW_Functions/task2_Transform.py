from enum import Enum
from task1_IsSorted import SortOrder, is_sorted


def transform(arr: list, order: Enum) -> list:
    return [i + num for i, num in zip(range(len(arr)), arr)] if is_sorted(arr, order) else arr


if __name__ == '__main__':
    num_arr = [int(i) for i in input('Please enter the array: ').split()]
    arr_set = [[5, 17, 24, 88, 33, 2],
               [15, 10, 3],
               [15, 10, 3],
               num_arr]

    for num_arr in arr_set:
        print(transform(num_arr, SortOrder.ASCENDING))

    for num_arr in arr_set:
        print(transform(num_arr, SortOrder.DESCENDING))

