from enum import Enum


class SortOrder(Enum):
    ASCENDING = 1
    DESCENDING = -1


def is_sorted(arr: list, order: Enum) -> bool:
    return all([arr[i] * order.value <= arr[i+1] * order.value for i in range(len(arr) - 1)])


if __name__ == '__main__':
    num_arr = [int(i) for i in input('Please enter the array: ').split()]

    print('Is sorted (ascending):', is_sorted(num_arr, SortOrder.ASCENDING))

    print('Is sorted (descending):', is_sorted(num_arr, SortOrder.DESCENDING))



