def mult_generator(a, t, n: int):
    mult_num = 1
    for i in range(n):
        mult_num *= a + t*i
        yield mult_num


def mult_arithmetic_elements(a, t, n: int) -> int:
    return [i for i in mult_generator(a, t, n)].pop(-1)


if __name__ == '__main__':
    print(mult_arithmetic_elements(5, 3, 4))
