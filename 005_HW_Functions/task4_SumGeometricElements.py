def sum_generator(a, alim: int, t: float):
    sum_num = 0
    while a > alim:
        sum_num += a
        yield sum_num
        a *= t


def sum_geometic_elements(a, alim: int, t: float) -> int:
        return [i for i in sum_generator(a, alim, t)].pop(-1) if 0 < t < 1 else None


if __name__ == '__main__':
    print(sum_geometic_elements(100, 20, 0.5))
