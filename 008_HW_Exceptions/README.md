# 008_HW_Exceptions

### task 1  
Here can raise TypeError  
The main factor is theoretical possibility to compare values of different types   
  
### task 2  
Here can raise NameError  

### task 3
Here can raise OSError  

### task 4
Here can raise SyntaxError  

### task 5
Here can raise TypeError  
  
### task 6
Here we can see infinite loop  


