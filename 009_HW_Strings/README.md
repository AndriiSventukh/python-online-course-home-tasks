# 009_HW_Strings

task1 concerning symbol ' " ' replace was done using two methods:
-	custom  function based on list comprehension,
-	custom function based on function map() and lambda function.  

task2 concerning palindrome was done using two different methods. The first one is based on using regular expression and specified function split(). The comparing method is pretty simple and based on using slicing expression. The second method based on filter() with lambda function and comparing is based on simple loop.  

task3 concerns the shortest word. Here was implemented simple filter() that adjusts incoming string by removing inappropriate characters from the string and applying lowercase to it.  

task4 was done by means of creating additional function string_handler,  and string module that make implementation of other function easier (simply in one line).  

task5 was done using the previous string_handler in a simpler form.  

* intricate and challenging solution added 5.11.20


