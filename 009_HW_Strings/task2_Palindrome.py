import re
# first step  in regexp was something like this r'''[\^.,:;'"’!?\s_\-()]''' instead of r'[\W_]' )), LoL
# but the main idea to get rid of "unnecessary" symbols


def is_palindrome_short(s: str) -> bool:
    s = ''.join(map(str, re.split(r'[\W_]', s))).lower()
    return s == s[::-1]


def is_palindrome_loop(s: str) -> bool:
    s = ''.join(filter(lambda x: x not in ''' !@#$%^&*()_+-=~`{}[]|\\/:";'<>,.?\n\t\r''', s)).lower()
    return all([s[i] == s[len(s) - i - 1] for i in range((len(s) // 2) - 1)])


if __name__ == '__main__':
    test_strings = ["Регулярка оказалась интересным вариантом в этом задании! ))",
                    "Аргентина манит негра! \n:))",
                    "\tMadam, I’m Adam",
                    "Was it a car or a cat I saw?",
                    "Rats live on no evil star",
                    "Лёша на полке клопа нашёл",
                    "Лё-ша на по^лке кло_па нашёл!",
                    "А роза упала на лапу Азора?",
                    "А ро\nза у\tпала, на, л-апу А^з!ора?",
                    "A910f019A"]

    print('Results using function'.upper(), 'is_palindrome_short(str):')
    for s_i in test_strings:
        print('Palindrome status: %5s  for string: %s' % (is_palindrome_short(s_i), s_i.__repr__()))

    print('\nResults using function'.upper(), 'is_palindrome_loop(str):')
    for s_i in test_strings:
        print('Palindrome status: %5s  for string: %s' % (is_palindrome_loop(s_i), s_i.__repr__()))


