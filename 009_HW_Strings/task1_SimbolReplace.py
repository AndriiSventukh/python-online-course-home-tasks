def symbol_replace1(s: str):
    return ''.join(['\'' if char == '"' else '\"' if char == '\'' else char for char in [ch for ch in s]])


def symbol_replace2(s: str):
    return ''.join(map(lambda ch: '\'' if ch == '"' else '\"' if ch == '\'' else ch, s))


if __name__ == '__main__':
    test_string = '''sdjvsslvs "sdcsc" acsma 'cmaskm' sdcsdvs ks "dksdk dsmsdvks" '''
    print(symbol_replace1(test_string))
    print(symbol_replace2(test_string))




