import string


def test_1_1(strings: list):
    return [key for key, value in string_handler(strings).items() if value == len(strings)]


def test_1_2(strings: list):
    return [key for key, value in string_handler(strings).items() if value > 0]


def test_1_3(strings: list):
    return [key for key, value in string_handler(strings).items() if value > 2]


def test_1_4(strings: list):
    return [key for key, value in string_handler(strings).items() if value == 0]


# a little bit intricate and challenging solution:
def string_handler(strings: list) -> dict:
    return {key: sum(value) for key, value in {key: [1 if key in str_i.lower() else 0 for str_i, i
            in zip(strings, range(len(strings)))] for key in string.ascii_lowercase}.items()} \
            if strings else {}

# Understandable solution beneath: (comment under and uncomment below)
# def string_handler(strings: list) -> dict:
#     if strings:
#         dict_array = []
#         result_dict = dict.fromkeys(string.ascii_lowercase, 0)
#
#         for str_i in strings:
#             letters_dict = dict.fromkeys(string.ascii_lowercase, 0)
#             for ch in str_i.lower():
#                 if ch in string.ascii_lowercase:
#                     letters_dict[ch] += 1
#             dict_array.append(letters_dict)
#
#         for dict_i in dict_array:
#             for ch in string.ascii_lowercase:
#                 result_dict[ch] += 1 if dict_i[ch] > 0 else 0
#
#         return result_dict
#     return {}


if __name__ == '__main__':

    test_strings = ["\tMadam, I’m Adam... or Smith",
                    "Was it a car or a cat I saw?",
                    "Rats live on no evil star",
                    "A910fo19A",
                    "Hello Ann",
                    "world is beautiful",
                    "python is all you need"]

    print(f'Characters that appear in all strings: {test_1_1(test_strings)}')
    print(f'Characters that appear at least in one string:{test_1_2(test_strings)}')
    print(f'Characters that appear at least in two strings:{test_1_3(test_strings)}')
    print(f'Characters that were not used in any string: {test_1_4(test_strings)}')

