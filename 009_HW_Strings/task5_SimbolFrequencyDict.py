import string


# a little bit intricate and challenging solution:
def count_letters1(cur_string: str) -> dict:
    return {key: value for key, value in {ch: cur_string.count(ch) for ch
            in string.ascii_lowercase}.items() if value > 0}


# Understandable solution beneath:
def count_letters2(cur_string: str) -> dict:
    result_dict = dict.fromkeys(string.ascii_lowercase, 0)
    for ch in cur_string.lower():
        if ch in string.ascii_lowercase:
            result_dict[ch] += 1
    return {key: value for key, value in result_dict.items() if value > 0}


if __name__ == '__main__':
    test_string = ' Lorem ipsum dolor sit amet, consectetur adipiscing: elit.'

    print(count_letters1(test_string))
    print(count_letters2(test_string))
