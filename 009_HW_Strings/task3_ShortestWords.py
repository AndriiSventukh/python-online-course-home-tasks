def get_shortest_word(s: str) -> str:
    shortest = ' ' * 1000
    temp_s = ''
    for ch in filter(lambda char: char not in '\n\t\r,.:-;"', s):
        if ch != ' ':
            temp_s = temp_s + ch
        else:
            if temp_s and len(temp_s) < len(shortest):
                shortest = temp_s
            temp_s = ''
    return shortest


if __name__ == '__main__':
    test_string = ' Lorem ipsum dolor sit amet, consectetur adipiscing: elit, sed do eiusmod tempor incididunt \n' \
                  'ut labore et dolore magna aliqua. Ut enim ad minim veniam; quis nostrud exercitation ullamco \t' \
                  'laboris nisi, ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in ' \
                  'voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat ' \
                  'non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. '
    print(f'The shortest word is: "{get_shortest_word(test_string)}"')


