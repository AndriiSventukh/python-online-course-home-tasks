import copy


class CustomList:
    next_node = None

    def __init__(self, *items):
        # next is used if initialisation by arbitrary set of values is needed
        if items:
            for it in items:
                self.append(it)

    def __add__(self, *items):
        # __add__ is being changed for the sake of creating possibility to simplify add operations
        if items:
            for it in items:
                self.append(it)
        return self

    def append(self, *data):
        # standard appending operation supposes appending items to the end of list
        cur_point = self
        while cur_point.next_node:
            cur_point = cur_point.next_node
        cur_point.next_node = ListNode(data)

    def insert(self, i, *data):
        # insert operation supposes finding the item positions and some other operations with objects
        cur_point = self

        # necessary check for index type
        if type(i) != int:
            raise ValueError

        # necessary check for negative index value and outbound case
        if (i < 0 and abs(i) > self.length()) or (i > 0 and i > self.length() - 1):
            raise IndexError

        if i > 0:
            while i:
                cur_point = cur_point.next_node
                i -= 1
        else:
            i = i + self.length()
            while i:
                cur_point = cur_point.next_node
                i -= 1
        temp_head = cur_point.next_node
        cur_point.next_node = None
        cur_point.next_node = ListNode(data)
        cur_point.next_node.next_node = temp_head

    def remove(self, *data):
        # remove operation supposes finding the item positions and some other operations with objects
        rem_data_node = ListNode(data)
        cur_point = self
        while cur_point.next_node:
            if cur_point.next_node.data != rem_data_node.data:
                cur_point = cur_point.next_node
            else:
                cur_point.next_node = cur_point.next_node.next_node
                break

    def pop(self, *pop_index):
        cur_point = self
        # necessary check if index value exists. This case has been considered
        if pop_index:
            pop_index = int(pop_index[0])

            if (pop_index < 0 and abs(pop_index) > self.length()) \
                    or (pop_index > 0 and pop_index > self.length() - 1):
                raise IndexError

            if pop_index > 0:
                # necessary check for negative index value. Both variants are implemented
                while pop_index:
                    cur_point = cur_point.next_node
                    pop_index -= 1
            else:
                pop_index = (pop_index + self.length()) % self.length()
                while pop_index:
                    cur_point = cur_point.next_node
                    pop_index -= 1
        else:
            # in case of index absence last value must be popped
            while cur_point.next_node.next_node:
                cur_point = cur_point.next_node
        pop_data = cur_point.next_node.data
        cur_point.next_node = cur_point.next_node.next_node
        return pop_data

    def index(self, data, *pos):
        # three variants are implemented? without start and end, with start and with both
        start_index = 0
        end_index = self.length()
        positions = [int(p) for p in pos]

        for p in positions:
            if (p < 0 and abs(p) > self.length()) \
                    or (p > 0 and p > self.length() - 1):
                raise IndexError

        if positions and positions[0] <= end_index:
            start_index = positions.pop(0)
        if positions and start_index <= positions[0] < end_index:
            end_index = positions.pop(0)
        cur_point = self
        cur_index = 0
        data_node = ListNode((data,))
        while start_index - cur_index:
            cur_point = cur_point.next_node
            cur_index += 1
        while cur_index < end_index:
            if cur_point.next_node.data == data_node.data:
                return cur_index
            else:
                cur_point = cur_point.next_node
                cur_index += 1
        for key, value in globals().items():
            if value is self:
                return f'{data_node} hasn\'t been found in presupposed area of \'{key}\''

    def count(self, data):
        cur_point = self
        counter = 0
        node = ListNode((data,))
        while cur_point.next_node:
            cur_point = cur_point.next_node
            if cur_point.data == node.data:
                counter += 1
        return counter

    def reverse(self):
        # was implemented by operations on nodes, not directly on values
        reversed_list = CustomList()
        cur_point = self
        while cur_point.next_node:
            temp_self = cur_point.next_node
            cur_point.next_node = cur_point.next_node.next_node
            temp_reversed = reversed_list.next_node
            reversed_list.next_node = temp_self
            reversed_list.next_node.next_node = temp_reversed
        self.next_node = reversed_list.next_node

    def length(self):
        cur_point = self
        counter = 0
        while cur_point.next_node:
            cur_point = cur_point.next_node
            counter += 1
        return counter

    def clear(self):
        self.next_node = None

    def __str__(self):
        if self.next_node:
            cur_point = self
            value_str = '['
            while cur_point.next_node:
                value_str += f', {cur_point.next_node}' if value_str[-1] != '[' \
                    else f'{cur_point.next_node}'
                cur_point = cur_point.next_node
            return value_str + ']'
        return '[]'

    def __iter__(self):
        # needed for object iteration
        self.iterated_node = self
        return self

    def __next__(self):
        # needed for object iteration
        if self.iterated_node.next_node:
            self.iterated_node = self.iterated_node.next_node
            return self.iterated_node.data
        else:
            del self.iterated_node
            raise StopIteration

    def __getitem__(self, key):
        start_index = 0
        end_index = self.length()
        step = 1
        # next check needed for slicing and access by index
        if isinstance(key, slice):
            if key.start:
                if type(key.start) == int:
                    if key.start >= 0:
                        if key.start < self.length():
                            start_index = key.start
                        else:
                            raise LookupError
                    else:
                        if 0 <= (key.start + self.length()) < self.length():
                            start_index = key.start + self.length()
                        else:
                            raise LookupError
                else:
                    raise ValueError
            if key.stop:
                if type(key.stop) == int:
                    if key.stop > 0:
                        if key.stop < self.length():
                            end_index = key.stop
                        else:
                            raise LookupError
                    else:
                        if 0 <= (key.stop + self.length()) < self.length():
                            end_index = key.stop + self.length()
                        else:
                            raise LookupError
                else:
                    raise ValueError
            if key.step:
                if type(key.step) == int:
                    step = key.step
                else:
                    raise ValueError
            if start_index == end_index:
                raise LookupError

            if start_index > end_index:
                start_index, end_index = end_index, start_index
            list_copy = copy.deepcopy(self)
            # print(list_copy)
            # print(start_index, end_index)
            if step < 0:
                list_copy.reverse()
                temp_start = start_index if start_index == 0 else start_index + 1
                temp_end = end_index if end_index == list_copy.length() else end_index + 1
                start_index = list_copy.length() - temp_end
                end_index = list_copy.length() - temp_start
                step *= -1
            # print(list_copy)
            # print(start_index, end_index)
            return_list = CustomList()
            cur_list_point = list_copy
            cur_index = 0
            while start_index - cur_index:
                cur_index += 1
                cur_list_point = cur_list_point.next_node

            counter = 0
            while cur_index < end_index:
                if counter % step == 0:
                    return_list.append(cur_list_point.next_node.data)
                cur_index += 1
                counter += 1
                cur_list_point = cur_list_point.next_node
            return return_list
        else:
            if type(key) == int:
                key = (key + self.length() * 2) % self.length()
                if not (0 <= key < self.length()):
                    raise LookupError
                list_copy = copy.deepcopy(self)
                return list_copy.pop(key)
            else:
                raise ValueError

    def __bool__(self):
        if self.next_node:
            print(self.next_node)
            return True
        return False


class ListNode:
    next_node = None

    def __init__(self, data) -> None:
        self.data = [d for d in data] if len(data) - 1 else data[0]

    def __str__(self):
        if isinstance(self.data, str):
            return f'\'{self.data}\''
        return f'{self.data}'


if __name__ == '__main__':
    # CustomList initialisation suppose ability to create list from eny set of items:
    test_list = CustomList('Hello!', 2, '3', 4, {14: 19}, [5], 'c', [1, 2, {2: 1}], 4, 1, '55', 6, '4') \
                + (1, 'c', {123: 65}, [0],) + {} + 18
    # Also after rewriting __add__ method such operation can be possible
    print(f'test_list:\n{test_list}')

    test_list.append('Python')
    test_list.append('is good')
    print(f'\ntest_list after append:\n{test_list}')

    test_list.insert(1, 'Dude')
    test_list.insert(-4, [])
    print(f'\ntest_list after insert:\n{test_list}')

    test_list.remove((1, 'c', {123: 65}, [0],))
    print(f'\ntest_list after remove:\n{test_list}')

    test_list.pop(-7)
    test_list.pop()
    print(f'\ntest_list after pop(-7) and pop():\n{test_list}')

    print(f'\nThe result of test_list.index(\'3\') is: {test_list.index("3")}')
    print(f'The result of test_list.index(4, 0, 7) is: {test_list.index(4, 0, 7)}')
    print(f"The result of test_list.index(\'111\') is: {test_list.index('111', 2)}")

    print(f'\nThe result of test_list.count(4) is: {test_list.count(4)}')

    print(f'\ntest_list before reverse: {test_list}')
    test_list.reverse()
    print(f'test_list  after reverse: {test_list}')

    print(f'\nThe CustomList length is: {test_list.length()}')

    # slicing and access by index is also available
    print(f'\nSliced by [::-16] test_list has next appearance: {test_list[::-16]}')
    print(f'Accessed by index [-2] test_list item has next value: {test_list[-2]}')

    # list comprehension is also available
    print(f'\nComprehended list: {[i for i in test_list]}')

    test_list.clear()
    print(f'\ntest_list after clear: {test_list}')
    print(f'Is list filled with data: {test_list.__bool__()}')
    print(f'\nThe length of cleared test list is: {test_list.length()}')
