def dictionary_combine(*dicts) -> dict:
    res = {}
    for dict_i in dicts:
        for key in dict_i.keys():
            if key in res.keys():
                res[key] += dict_i[key]
            else:
                res.update({key: dict_i.get(key)})
    return res


if __name__ == '__main__':
    dict_1 = {'a': 100, 'b': 200}
    dict_2 = {'a': 200, 'c': 300}
    dict_3 = {'a': 300, 'd': 100}

    print(dictionary_combine(dict_1, dict_2))
    print(dictionary_combine(dict_1, dict_2, dict_3))
