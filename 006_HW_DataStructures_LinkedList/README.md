# 006_HW_DataStructures_LinkedList

task1 has been done simply using conditions end rewriting value by update function or adding elements to dictionary  
  
task2 CustomList has been implemented using two classes:
- CustomList
- ListNode

CustomList provides the possibility to create list from any other set of data  
Also there is a possibility to add any set of data to current list simply using '+'  
All existing in ordinary lists methods accepting sort() was implemented in CustomList  
To make CustomList iterable the __iter__ and __next__ methods have been added to <CustomList> class  
Also __str__ methods in CustomList class and ListNode helps to receive data in convenient form  

Slicing and access by index are also added by means of __getitem__ method (3.11.12)
Exceptions added (16.11.20)



